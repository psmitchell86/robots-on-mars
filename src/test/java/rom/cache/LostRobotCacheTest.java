package rom.cache;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rom.model.Orientation;
import rom.model.RobotAttitude;
import rom.model.State;

public class LostRobotCacheTest {
    private ILostRobotCache lostRobotCache;

    @Before
    public void setUp() {
        lostRobotCache = new LostRobotCache();
    }

    @Test
    public void getLostRobotsLastKnownAttitudes_initiallyEmpty() {
        // assert
        Assert.assertEquals(0, lostRobotCache.getLostRobotsLastKnownAttitudes().size());
    }

    @Test
    public void getLostRobotsLastKnownAttitudes_containsAddedValues() {
        // setup
        RobotAttitude robotAttitude = new RobotAttitude(1, 1, Orientation.NORTH, State.LOST);

        // test
        lostRobotCache.addLostRobotLastKnownAttitude(robotAttitude);

        // assert
        Assert.assertEquals(1, lostRobotCache.getLostRobotsLastKnownAttitudes().size());
        Assert.assertTrue(lostRobotCache.getLostRobotsLastKnownAttitudes().contains(robotAttitude));
    }

    @Test(expected = IllegalStateException.class)
    public void addLostRobotLastKnownAttitude_addAlivePosition_fail() {
        // setup
        RobotAttitude robotAttitude = new RobotAttitude(1, 1, Orientation.NORTH, State.ALIVE);

        // test
        lostRobotCache.addLostRobotLastKnownAttitude(robotAttitude);
    }
}
