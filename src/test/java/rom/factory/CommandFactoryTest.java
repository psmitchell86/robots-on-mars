package rom.factory;

import mockit.Mocked;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.processor.command.CommandProcessorBase;
import rom.processor.command.ForwardCommandProcessor;
import rom.processor.command.LeftCommandProcessor;
import rom.processor.command.RightCommandProcessor;

import java.util.ArrayList;
import java.util.List;

public class CommandFactoryTest {
    @Mocked
    private ILostRobotCache deadRobotCache;

    private ICommandFactory commandFactory;

    private LeftCommandProcessor leftCommandProcessor;
    private RightCommandProcessor rightCommandProcessor;
    private ForwardCommandProcessor forwardCommandProcessor;
    private static final int MAX_X_COORDINATE = 5;
    private static final int MAX_Y_COORDINATE = 10;

    @Before
    public void setUp() {
        leftCommandProcessor = new LeftCommandProcessor(MAX_X_COORDINATE, MAX_Y_COORDINATE, deadRobotCache);
        rightCommandProcessor = new RightCommandProcessor(MAX_X_COORDINATE, MAX_Y_COORDINATE, deadRobotCache);
        forwardCommandProcessor = new ForwardCommandProcessor(MAX_X_COORDINATE, MAX_Y_COORDINATE, deadRobotCache);

        List<CommandProcessorBase> commandProcessors = new ArrayList<>();
        commandProcessors.add(leftCommandProcessor);
        commandProcessors.add(rightCommandProcessor);
        commandProcessors.add(forwardCommandProcessor);

        commandFactory = new CommandFactory(commandProcessors);
    }

    @Test
    public void leftCommandAcronym_success() {
        // test
        CommandProcessorBase result = commandFactory.getCommandProcessor(Command.LEFT.getCommandAcronym());

        // assert
        Assert.assertTrue(result instanceof LeftCommandProcessor);
    }

    @Test
    public void rightCommandAcronym_success() {
        // test
        CommandProcessorBase result = commandFactory.getCommandProcessor(Command.LEFT.getCommandAcronym());

        // assert
        Assert.assertTrue(result instanceof LeftCommandProcessor);
    }

    @Test
    public void forwardCommandAcronym_success() {
        // test
        CommandProcessorBase result = commandFactory.getCommandProcessor(Command.LEFT.getCommandAcronym());

        // assert
        Assert.assertTrue(result instanceof LeftCommandProcessor);
    }

    @Test(expected = IllegalStateException.class)
    public void invalidCommandAcronym_fail() {
        // test
        commandFactory.getCommandProcessor("somethingElse");
    }
}
