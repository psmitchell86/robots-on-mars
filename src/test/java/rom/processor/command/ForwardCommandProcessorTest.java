package rom.processor.command;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.model.Orientation;
import rom.model.RobotAttitude;
import rom.model.State;

import java.util.HashSet;
import java.util.Set;

@RunWith(JMockit.class)
public class ForwardCommandProcessorTest {
    @Mocked
    private ILostRobotCache deadRobotCache;

    private ForwardCommandProcessor forwardCommandProcessor;

    private RobotAttitude robotAttitude;
    private static final int MAX_X_COORDINATE = 5;
    private static final int MAX_Y_COORDINATE = 10;
    private static final int X_COORDINATE = 3;
    private static final int Y_COORDINATE = 4;

    @Before
    public void setUp() {
        forwardCommandProcessor = new ForwardCommandProcessor(MAX_X_COORDINATE, MAX_Y_COORDINATE, deadRobotCache);
    }

    @Test
    public void getProcessableCommand() {
        // assert
        Assert.assertEquals(Command.FORWARD, forwardCommandProcessor.getProcessableCommand());
    }

    @Test
    public void processCommand_OrientationNorth() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.NORTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE + 1, Orientation.NORTH, State.ALIVE);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationEast() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE + 1, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationSouth() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.SOUTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE - 1, Orientation.SOUTH, State.ALIVE);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationWest() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE - 1, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_edgeNorth_robotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, MAX_Y_COORDINATE, Orientation.NORTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, MAX_Y_COORDINATE, Orientation.NORTH, State.LOST);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 1;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_previousLoss_edgeNorth_robotNotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, MAX_Y_COORDINATE, Orientation.NORTH, State.ALIVE);
        RobotAttitude existingDeadRobot = new RobotAttitude(X_COORDINATE, MAX_Y_COORDINATE, Orientation.NORTH, State.LOST);
        final Set<RobotAttitude> deadRobots = new HashSet<RobotAttitude>();
        deadRobots.add(existingDeadRobot);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, MAX_Y_COORDINATE, Orientation.NORTH, State.ALIVE);

        // setup mocks
        new Expectations() {{
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            result = deadRobots;
        }};

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_edgeEast_robotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(MAX_X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(MAX_X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.LOST);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 1;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_previousLoss_edgeEast_robotNotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(MAX_X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);
        RobotAttitude existingDeadRobot = new RobotAttitude(MAX_X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.LOST);
        final Set<RobotAttitude> deadRobots = new HashSet<RobotAttitude>();
        deadRobots.add(existingDeadRobot);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(MAX_X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // setup mocks
        new Expectations() {{
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            result = deadRobots;
        }};

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_edgeSouth_robotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, 0, Orientation.SOUTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, 0, Orientation.SOUTH, State.LOST);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 1;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_previousLoss_edgeSouth_robotNotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, 0, Orientation.SOUTH, State.ALIVE);
        RobotAttitude existingDeadRobot = new RobotAttitude(X_COORDINATE, 0, Orientation.SOUTH, State.LOST);
        final Set<RobotAttitude> deadRobots = new HashSet<RobotAttitude>();
        deadRobots.add(existingDeadRobot);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, 0, Orientation.SOUTH, State.ALIVE);

        // setup mocks
        new Expectations() {{
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            result = deadRobots;
        }};

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_edgeWest_robotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(0, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(0, Y_COORDINATE, Orientation.WEST, State.LOST);

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 1;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_previousLoss_edgeWest_robotNotLost() {
        // setup test data
        robotAttitude = new RobotAttitude(0, Y_COORDINATE, Orientation.WEST, State.ALIVE);
        RobotAttitude existingDeadRobot = new RobotAttitude(0, Y_COORDINATE, Orientation.WEST, State.LOST);
        final Set<RobotAttitude> deadRobots = new HashSet<RobotAttitude>();
        deadRobots.add(existingDeadRobot);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(0, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // setup mocks
        new Expectations() {{
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            result = deadRobots;
        }};

        // test
        RobotAttitude actualResult = forwardCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 1;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }
}
