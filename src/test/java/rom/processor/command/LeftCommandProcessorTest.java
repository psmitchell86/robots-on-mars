package rom.processor.command;

import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.model.Orientation;
import rom.model.RobotAttitude;
import rom.model.State;

@RunWith(JMockit.class)
public class LeftCommandProcessorTest {
    @Mocked
    private ILostRobotCache deadRobotCache;

    private LeftCommandProcessor leftCommandProcessor;

    private RobotAttitude robotAttitude;
    private static final int MAX_X_COORDINATE = 5;
    private static final int MAX_Y_COORDINATE = 10;
    private static final int X_COORDINATE = 5;
    private static final int Y_COORDINATE = 10;

    @Before
    public void setUp() {
        leftCommandProcessor = new LeftCommandProcessor(MAX_X_COORDINATE, MAX_Y_COORDINATE, deadRobotCache);
    }

    @Test
    public void getProcessableCommand() {
        // assert
        Assert.assertEquals(Command.LEFT, leftCommandProcessor.getProcessableCommand());
    }

    @Test
    public void processCommand_OrientationNorth() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.NORTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // test
        RobotAttitude actualResult = leftCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationEast() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.NORTH, State.ALIVE);

        // test
        RobotAttitude actualResult = leftCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationSouth() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.SOUTH, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.EAST, State.ALIVE);

        // test
        RobotAttitude actualResult = leftCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void processCommand_OrientationWest() {
        // setup test data
        robotAttitude = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.WEST, State.ALIVE);

        // setup expected data
        RobotAttitude expectedResult = new RobotAttitude(X_COORDINATE, Y_COORDINATE, Orientation.SOUTH, State.ALIVE);

        // test
        RobotAttitude actualResult = leftCommandProcessor.processCommand(robotAttitude);

        // verify mocks
        new Verifications() {{
            deadRobotCache.addLostRobotLastKnownAttitude((RobotAttitude) any);
            times = 0;
            deadRobotCache.getLostRobotsLastKnownAttitudes();
            times = 0;
        }};

        // assert
        Assert.assertEquals(expectedResult, actualResult);
    }
}
