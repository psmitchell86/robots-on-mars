package rom.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RobotAttitudeTest {
    private RobotAttitude robotAttitude1;
    private RobotAttitude robotAttitude2;
    private RobotAttitude robotAttitude3;

    private static final int POSITION_1 = 1;
    private static final int POSITION_2 = 2;
    private static final int POSITION_3 = 3;

    @Before
    public void setUp() {
        robotAttitude1 = new RobotAttitude(POSITION_1, POSITION_2, Orientation.NORTH, State.ALIVE);
        robotAttitude2 = new RobotAttitude(POSITION_1, POSITION_2, Orientation.NORTH, State.ALIVE);
        robotAttitude3 = new RobotAttitude(POSITION_3, POSITION_2, Orientation.NORTH, State.LOST);
    }


    @Test
    public void equals_success() {
        // test
        boolean result = robotAttitude1.equals(robotAttitude2);

        // assert
        Assert.assertTrue(result);
    }

    @Test
    public void equals_fail() {
        // test
        boolean result = robotAttitude1.equals(robotAttitude3);

        // assert
        Assert.assertFalse(result);
    }

    @Test
    public void hashcode_success() {
        // assert
        Assert.assertEquals(robotAttitude1.hashCode(), robotAttitude2.hashCode());
    }

    @Test
    public void hashcode_fail() {
        // assert
        Assert.assertNotEquals(robotAttitude1.hashCode(), robotAttitude3.hashCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_nullOrientation() {
        // test
        new RobotAttitude(POSITION_1, POSITION_2, null, State.LOST);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_nullState() {
        // test
        new RobotAttitude(POSITION_1, POSITION_2, Orientation.NORTH, null);
    }
}
