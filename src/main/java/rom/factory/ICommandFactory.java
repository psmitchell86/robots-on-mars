package rom.factory;

import rom.processor.command.CommandProcessorBase;

public interface ICommandFactory {

    CommandProcessorBase getCommandProcessor(String commandAcronym);
}
