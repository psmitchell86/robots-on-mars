package rom.factory;

import rom.processor.command.CommandProcessorBase;

import java.util.List;
import java.util.Optional;

public class CommandFactory implements ICommandFactory {
    private final List<CommandProcessorBase> commandProcessors;

    public CommandFactory(List<CommandProcessorBase> commandProcessors) {
        this.commandProcessors = commandProcessors;
    }

    public CommandProcessorBase getCommandProcessor(final String commandAcronym) {
        Optional<CommandProcessorBase> commandProcessor = commandProcessors
                .stream()
                .filter(cp -> cp.getProcessableCommand().getCommandAcronym().equals(commandAcronym))
                .findFirst();

        if(commandProcessor.isPresent()) {
            return commandProcessor.get();
        } else {
            throw new IllegalStateException(String.format("Unknown command %s", commandAcronym));
        }
    }
}
