package rom;

import rom.cache.ILostRobotCache;
import rom.cache.LostRobotCache;
import rom.factory.CommandFactory;
import rom.factory.ICommandFactory;
import rom.model.Input;
import rom.model.Orientation;
import rom.model.RobotAttitude;
import rom.model.State;
import rom.processor.command.CommandProcessorBase;
import rom.processor.command.ForwardCommandProcessor;
import rom.processor.command.LeftCommandProcessor;
import rom.processor.command.RightCommandProcessor;
import rom.processor.input.IInputProcessor;
import rom.processor.input.InputProcessor;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        // if(args.length != 2) {
        //     throw new IllegalArgumentException("Need input file and output file location");
        // }

        // initialisation
        // IInputProcessor inputProcessor = new InputProcessor();
        // Input input = inputProcessor.getInput(args[0]);

        List<RobotAttitude> startingRobotAttitudes = new ArrayList<>();
        startingRobotAttitudes.add(new RobotAttitude(1, 1, Orientation.EAST, State.ALIVE));
        startingRobotAttitudes.add(new RobotAttitude(3, 2, Orientation.NORTH, State.ALIVE));
        startingRobotAttitudes.add(new RobotAttitude(0,3, Orientation.WEST, State.ALIVE));

        List<String> robotInstructions = new ArrayList<>();
        robotInstructions.add("RFRFRFRF");
        robotInstructions.add("FRRFLLFFRRFLL");
        robotInstructions.add("LLFFFLFLFL");

        Input input = new Input(5, 3, startingRobotAttitudes, robotInstructions);

        ILostRobotCache lostRobotCache = new LostRobotCache();

        LeftCommandProcessor leftCommandProcessor = new LeftCommandProcessor(input.getMaxXCoordinate(), input.getMaxYCoordinate(), lostRobotCache);
        RightCommandProcessor rightCommandProcessor = new RightCommandProcessor(input.getMaxXCoordinate(), input.getMaxYCoordinate(), lostRobotCache);
        ForwardCommandProcessor forwardCommandProcessor = new ForwardCommandProcessor(input.getMaxXCoordinate(), input.getMaxYCoordinate(), lostRobotCache);

        List<CommandProcessorBase> commandProcessors = new ArrayList<>();
        commandProcessors.add(leftCommandProcessor);
        commandProcessors.add(rightCommandProcessor);
        commandProcessors.add(forwardCommandProcessor);

        ICommandFactory commandFactory = new CommandFactory(commandProcessors);

        List<RobotAttitude> outputRobotAttitudes  = new ArrayList<>();

        // run
        for(int i = 0; i < input.getStartingRobotAttitudes().size(); i++) {
            RobotAttitude newRobotAttitude = input.getStartingRobotAttitudes().get(i);
            for (int j = 0; j < input.getRobotInstructions().get(i).length(); j++){
                String command = String.valueOf(input.getRobotInstructions().get(i).charAt(j));

                CommandProcessorBase commandProcessor = commandFactory.getCommandProcessor(command);
                if(newRobotAttitude.getState() != State.LOST) {
                    newRobotAttitude = commandProcessor.processCommand(newRobotAttitude);
                    if(newRobotAttitude.getState() == State.LOST) {
                        break;
                    }
                }
            }
            outputRobotAttitudes.add(newRobotAttitude);
            System.out.println(newRobotAttitude.toString());
        }
    }
}
