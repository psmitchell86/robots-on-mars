package rom.processor.input;

import org.apache.commons.io.FileUtils;
import rom.model.Input;
import rom.model.Orientation;
import rom.model.RobotAttitude;
import rom.model.State;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class InputProcessor implements IInputProcessor {

    @Override
    public Input getInput(String inputFileLocation) {
        int maxXCoordinate = 0, maxYCoordinate = 0;
        List<RobotAttitude> robotAttitudes = new ArrayList<>();
        List<String> robotInstructions = new ArrayList<>();

        File inputFile = new File(inputFileLocation);
        if (!inputFile.exists() || !inputFile.isFile()) {
            throw new IllegalArgumentException("Input file does not exist!");
        }

        List<String> lines = null;

        try {
            lines = FileUtils.readLines(inputFile);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to read input file");
        }

        lines = lines.stream()
                .filter(l -> !l.matches("\\R"))
                .collect(Collectors.toList());

        if(lines.size() < 3 && lines.size() % 2 != 1) {
            throw new IllegalArgumentException("should be at least 3 lines or and odd number of lines");
        }

        Matcher matcher = Pattern
                .compile("^([0-9]|[1-9][0-9]|100) (0-9]|[1-9][0-9]|100)$")
                .matcher(lines.get(0));

        if(!matcher.matches()) {
            throw new IllegalArgumentException("Failed to read map max coordinates");
        }

        while(matcher.find()) {
            maxXCoordinate = Integer.parseInt(matcher.group(1));
            maxYCoordinate = Integer.parseInt(matcher.group(2));
        }

        lines.remove(0);

        for(int i = 0; i < lines.size(); i = i + 2) {
            int startPositionX = 0, startPositionY = 0;
            Orientation orientation = null;
            String robotInstruction = null;
            Matcher robotPositionMatcher = Pattern.compile("^([0-9]|[1-9][0-9]|100) (0-9]|[1-9][0-9]|100) ([NESW])$").matcher(lines.get(i));
            Matcher robotInstructionMatcher = Pattern.compile("^([LRF]+)$").matcher(lines.get(i + 1));

            if(!robotPositionMatcher.matches()) {
                throw new IllegalArgumentException("Failed to get initial robot position");
            }

            if(!robotPositionMatcher.matches()) {
                throw new IllegalArgumentException("Failed to get robot instruction set");
            }

            while(robotPositionMatcher.find()) {
                startPositionX = Integer.parseInt(matcher.group(1));
                startPositionY = Integer.parseInt(matcher.group(2));
                switch (matcher.group(3)) {
                    case "N":
                        orientation = Orientation.NORTH;
                        break;
                    case "E":
                        orientation = Orientation.EAST;
                        break;
                    case "S":
                        orientation = Orientation.SOUTH;
                        break;
                    case "W":
                        orientation = Orientation.WEST;
                        break;
                }
            }

            if (startPositionX > maxXCoordinate || startPositionY > maxYCoordinate) {
                throw new IllegalArgumentException("start position is off grid");
            }

            while (robotInstructionMatcher.find()) {
                robotInstruction = matcher.group(1);
            }

            robotAttitudes.add(new RobotAttitude(startPositionX, startPositionY, orientation, State.ALIVE));
            robotInstructions.add(robotInstruction);
        }

        return new Input(maxXCoordinate, maxYCoordinate, robotAttitudes, robotInstructions);
    }
}
