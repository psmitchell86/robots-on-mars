package rom.processor.input;

import rom.model.Input;

public interface IInputProcessor {
    Input getInput(String inputFileLocation);
}
