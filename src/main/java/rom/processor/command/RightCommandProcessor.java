package rom.processor.command;

import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.model.Orientation;
import rom.model.RobotAttitude;

public class RightCommandProcessor extends CommandProcessorBase {
    public RightCommandProcessor(int maxGridX, int maxGridY, ILostRobotCache lostRobotCache) {
        super(maxGridX, maxGridY, lostRobotCache);
    }

    protected RobotAttitude getNextRobotAttitude(RobotAttitude currentAttitude) {
        Orientation newOrientation = null;

        switch (currentAttitude.getOrientation()) {
            case NORTH:
                newOrientation = Orientation.EAST;
                break;
            case EAST:
                newOrientation = Orientation.SOUTH;
                break;
            case SOUTH:
                newOrientation = Orientation.WEST;
                break;
            case WEST:
                newOrientation = Orientation.NORTH;
                break;
            default:
                throw new IllegalStateException("Someone added to the orientation enum");
        }


        return new RobotAttitude(currentAttitude.getPositionX(), currentAttitude.getPositionY(), newOrientation, currentAttitude.getState());
    }

    public Command getProcessableCommand() {
        return Command.RIGHT;
    }
}
