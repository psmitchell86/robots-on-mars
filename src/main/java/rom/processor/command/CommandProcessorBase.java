package rom.processor.command;

import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.model.RobotAttitude;
import rom.model.State;

public abstract class CommandProcessorBase{
    private final int maxGridX;
    private final int maxGridY;
    private final ILostRobotCache lostRobotCache;

    protected CommandProcessorBase(int maxGridX, int maxGridY, ILostRobotCache lostRobotCache) {
        this.maxGridX = maxGridX;
        this.maxGridY = maxGridY;
        this.lostRobotCache = lostRobotCache;
    }

    protected abstract RobotAttitude getNextRobotAttitude(RobotAttitude currentAttitude);

    public abstract Command getProcessableCommand();

    public RobotAttitude processCommand(RobotAttitude currentAttitude) {
        RobotAttitude nextPosition = getNextRobotAttitude(currentAttitude);

        if(nextPosition.getPositionX() > maxGridX ||
                nextPosition.getPositionX() < 0 ||
                nextPosition.getPositionY() > maxGridY ||
                nextPosition.getPositionY() < 0) {
            // the robot is now lost
            RobotAttitude lostRobotAttitude = new RobotAttitude(currentAttitude.getPositionX(),
                    currentAttitude.getPositionY(),
                    currentAttitude.getOrientation(),
                    State.LOST);

            if(lostRobotCache.getLostRobotsLastKnownAttitudes().contains(lostRobotAttitude)) {
                // ignore the command as something was already lost at this last known location
                nextPosition = currentAttitude;
            } else {
                // this is the first time its been lost - add to cache
                nextPosition = lostRobotAttitude;
                lostRobotCache.addLostRobotLastKnownAttitude(lostRobotAttitude);
            }
        }

        return nextPosition;
    }
}
