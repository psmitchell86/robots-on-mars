package rom.processor.command;

import rom.cache.ILostRobotCache;
import rom.model.Command;
import rom.model.RobotAttitude;

public class ForwardCommandProcessor extends CommandProcessorBase {
    public ForwardCommandProcessor(int maxGridX, int maxGridY, ILostRobotCache lostRobotCache) {
        super(maxGridX, maxGridY, lostRobotCache);
    }

    protected RobotAttitude getNextRobotAttitude(RobotAttitude currentAttitude) {
        int newPositionX = currentAttitude.getPositionX();
        int newPositionY = currentAttitude.getPositionY();

        switch (currentAttitude.getOrientation()) {
            case NORTH:
                newPositionY ++;
                break;
            case EAST:
                newPositionX ++;
                break;
            case SOUTH:
                newPositionY--;
                break;
            case WEST:
                newPositionX--;
                break;
            default:
                throw new IllegalStateException("Someone added to the orientation enum");
        }


        return new RobotAttitude(newPositionX, newPositionY, currentAttitude.getOrientation(), currentAttitude.getState());
    }

    public Command getProcessableCommand() {
        return Command.FORWARD;
    }
}
