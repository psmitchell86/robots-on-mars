package rom.cache;

import rom.model.RobotAttitude;

import java.util.Set;

public interface ILostRobotCache {
    void addLostRobotLastKnownAttitude(RobotAttitude robotAttitude);

    Set<RobotAttitude> getLostRobotsLastKnownAttitudes();
}
