package rom.cache;

import rom.model.RobotAttitude;
import rom.model.State;

import java.util.HashSet;
import java.util.Set;

public class LostRobotCache implements ILostRobotCache {
    private final Set<RobotAttitude> deadRobots;

    public LostRobotCache() {
        deadRobots = new HashSet<>();
    }

    @Override
    public void addLostRobotLastKnownAttitude(RobotAttitude robotAttitude) {
        if(robotAttitude.getState() != State.LOST) {
            throw new IllegalStateException("Cannot add alive robots to lost cache!");
        }

        deadRobots.add(robotAttitude);
    }

    @Override
    public Set<RobotAttitude> getLostRobotsLastKnownAttitudes() {
        return deadRobots;
    }
}
