package rom.model;

public enum Orientation {
    NORTH("N"),
    EAST("E"),
    SOUTH("S"),
    WEST("W");

    private final String orientationAcronym;

    Orientation(String orientationAcronym) {
        this.orientationAcronym = orientationAcronym;
    }

    public String getOrientationAcronym() {
        return orientationAcronym;
    }
}
