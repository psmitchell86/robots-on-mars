package rom.model;

public class RobotAttitude {
    private final int positionX;
    private final int positionY;
    private final Orientation orientation;
    private final State state;


    public RobotAttitude(int positionX, int positionY, Orientation orientation, State state) {
        if(orientation == null)
            throw new IllegalArgumentException("orientation");

        if (state == null)
            throw new IllegalArgumentException("state");

        this.positionX = positionX;
        this.positionY = positionY;
        this.orientation = orientation;
        this.state = state;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public State getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RobotAttitude that = (RobotAttitude) o;

        if (positionX != that.positionX) return false;
        if (positionY != that.positionY) return false;
        if (orientation != that.orientation) return false;
        return state == that.state;
    }

    @Override
    public int hashCode() {
        int result = positionX;
        result = 31 * result + positionY;
        result = 31 * result + orientation.hashCode();
        result = 31 * result + state.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "RobotAttitude{" +
                "positionX=" + positionX +
                ", positionY=" + positionY +
                ", orientation=" + orientation +
                ", state=" + state +
                '}';
    }
}
