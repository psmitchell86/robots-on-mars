package rom.model;

import java.util.List;

public class Input {
    private final int maxXCoordinate;
    private final int maxYCoordinate;
    private final List<RobotAttitude> startingRobotAttitudes;
    private final List<String> robotInstructions;

    public Input(int maxXCoordinate, int maxYCoordinate, List<RobotAttitude> startingRobotAttitudes, List<String> robotInstructions) {
        this.maxXCoordinate = maxXCoordinate;
        this.maxYCoordinate = maxYCoordinate;
        this.startingRobotAttitudes = startingRobotAttitudes;
        this.robotInstructions = robotInstructions;
    }

    public int getMaxXCoordinate() {
        return maxXCoordinate;
    }

    public int getMaxYCoordinate() {
        return maxYCoordinate;
    }

    public List<RobotAttitude> getStartingRobotAttitudes() {
        return startingRobotAttitudes;
    }

    public List<String> getRobotInstructions() {
        return robotInstructions;
    }
}
