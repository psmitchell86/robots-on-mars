package rom.model;

public enum Command {
    LEFT("L"),
    RIGHT("R"),
    FORWARD("F");

    private final String commandAcronym;

    Command(String commandAcronym) {
        this.commandAcronym = commandAcronym;
    }

    public String getCommandAcronym() {
        return commandAcronym;
    }
}
