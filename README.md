# Robots on Mars
## Requirements

 - Maven 3.5.2
 - Java 1.8.0_162

## Build

    mvn clean verify

## Run

    java -jar ./target/robots-on-mars-1.0-SNAPSHOT.jar

You should see the following output

    RobotAttitude{positionX=1, positionY=1, orientation=EAST, state=ALIVE}
    RobotAttitude{positionX=3, positionY=3, orientation=NORTH, state=LOST}
    RobotAttitude{positionX=2, positionY=3, orientation=SOUTH, state=ALIVE}
